/**
 * 
 */
package com.terence.application;

import com.terence.entites.Adresse;
import com.terence.entites.Personne;
import static com.terence.utilitaires.Outils.*;

/**
 * @author User-05
 *
 */
public class Principale {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		Adresse paname = new Adresse(2, "« rue Victor Hugo »", 75008, "« Paris »");
//		Adresse nantes = new Adresse(17, "« rue de la République »", 44000, "« Nantes »");
		Adresse lille = new Adresse(55, "« Bld de la Libération »", 59000, "« Lille »");
		
//		System.out.println(panam);
//		System.out.println(nantes);
//		System.out.println(lille);
//		
//		JOptionPane.showMessageDialog(null, panam);
//		JOptionPane.showMessageDialog(null, nantes);
//		JOptionPane.showMessageDialog(null, lille);
		
		Personne jean = new Personne("Jean", "Dupont", 30, lille);
//		Personne bernard = new Personne("Bernard", "Morin", 45, paname);
//		Personne nathalie = new Personne("Nathalie", "Durand", 35, nantes);
//		
//		JOptionPane.showMessageDialog(null, jean);
//		JOptionPane.showMessageDialog(null, bernard);
//		JOptionPane.showMessageDialog(null, nathalie);
//		
		jean.setAdresse(new Adresse(44, "Rue des Docks", 33000, "Bordeaux"));
//		bernard.setAdresse(new Adresse(2, "« rue Victor Hugo »", 75008, "« Paris »"));
//		nathalie.setAdresse(new Adresse(2, "« rue Victor Hugo »", 75008, "« Paris »"));
//		JOptionPane.showMessageDialog(null, jean);
//		JOptionPane.showMessageDialog(null, nathalie + "\n" + bernard);
		
		afficher(jean);

	}

}
