package com.terence.entites;

public class Adresse {
	private int numero_rue;
	private String nom_rue;
	private int codePostal;
	private String ville;
	
	public Adresse(int pNumero_rue, String pNom_rue, int pCodePostal, String pVille) {
		this.numero_rue = pNumero_rue;
		this.nom_rue = pNom_rue;
		this.codePostal = pCodePostal;
		this.ville = pVille;
	}

	@Override
	public String toString() {
		return "[" + numero_rue 
				+ ", " 
				+ nom_rue 
				+ ", " 
				+ codePostal 
				+ ", "
				+ ville + "]";
	}

	public int getNumero_rue() {
		return numero_rue;
	}

	public void setNumero_rue(int sNumero_rue) {
		this.numero_rue = sNumero_rue;
	}

	public String getNom_rue() {
		return nom_rue;
	}

	public void setNom_rue(String sNom_rue) {
		this.nom_rue = sNom_rue;
	}

	public int getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(int sCodePostal) {
		this.codePostal = sCodePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String sVille) {
		this.ville = sVille;
	}
}
