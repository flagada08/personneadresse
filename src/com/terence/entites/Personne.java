package com.terence.entites;

public class Personne {
	private String prenom;
	private String nom;
	private int age;
	private Adresse	adresse;
	
	/**
	 * @param pPrenom
	 * @param pNom
	 * @param pAge
	 * @param pAdresse
	 */
	public Personne(String pPrenom, String pNom, int pAge, Adresse pAdresse) {
		super();
		this.prenom = pPrenom;
		this.nom = pNom;
		this.age = pAge;
		this.adresse = pAdresse;
	}

	@Override
	public String toString() {
		return prenom + " " + nom + " " + age + " ans, habite au " + adresse;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the adresse
	 */
	public Adresse getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
}
